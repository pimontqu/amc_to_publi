import csv

path_amc_file = 'assets/input/p2020/amc_out.csv'
path_tutor_file = 'assets/input/p2020/mail_tutor.csv'
path_merge_file = 'assets/input/p2020/amc_final.csv'
header_merge_file = ['Nom', 'surname', 'Tuteur', 'Email', 'note','206','208','9714','BA1301','BA1501','BA2101','Bary22','Bary32','FLVH1','Geo3d21','Geo3d41','OG1','OG32','Phvec52',
'Poutres12','Poutres41','VH10','VH6','VHb2','VHbis4','Vitesse31','algo1','algo2','assert1','assert2','ella','fc1','fdp1','lecture11','lecture31','mathbis131','maths115','maths12','maths123','maths126',
'maths17','maths19','maths224','maths225','mathsbis11','mathsbis111','mathsbis119','mathsbis120','mathsbis16','mathsbis215','mathsbis28b','mp22','mp42','phrec21','phrec41','pytha42','pytha43','testlog1',
'thales32','thales42','vec82']

with open(path_merge_file, 'w') as merge_file:
    merge_writer = csv.DictWriter(merge_file, fieldnames = header_merge_file, quoting = csv.QUOTE_NONNUMERIC)
    merge_writer.writeheader()
    with open(path_amc_file) as amc_file:
        amc_reader = csv.reader(amc_file, delimiter=',')
        for id_row_amc, row_amc in enumerate(amc_reader):
            if(id_row_amc == 0):
                header_amc_csv = {
                    'name' : row_amc.index('Nom'),
                    'note' : row_amc.index('note'),
                    '206' : row_amc.index('206'),
                    '208' : row_amc.index('208'),
                    '9714' : row_amc.index('9714'),
                    'BA1301' : row_amc.index('BA1301'),
                    'BA1501' : row_amc.index('BA1501'),
                    'BA2101' : row_amc.index('BA2101'),
                    'Bary22' : row_amc.index('Bary22'),
                    'Bary32' : row_amc.index('Bary32'),
                    'FLVH1' : row_amc.index('FLVH1'),
                    'Geo3d21' : row_amc.index('Geo3d21'),
                    'Geo3d41' : row_amc.index('Geo3d41'),
                    'OG1' : row_amc.index('OG1'),
                    'OG32' : row_amc.index('OG32'),
                    'Phvec52' : row_amc.index('Phvec52'),
                    'Poutres12' : row_amc.index('Poutres12'),
                    'Poutres41' : row_amc.index('Poutres41'),
                    'VH10' : row_amc.index('VH10'),
                    'VH6' : row_amc.index('VH6'),
                    'VHb2' : row_amc.index('VHb2'),
                    'VHbis4' : row_amc.index('VHbis4'),
                    'Vitesse31' : row_amc.index('Vitesse31'),
                    'algo1' : row_amc.index('algo1'),
                    'algo2' : row_amc.index('algo2'),
                    'assert1' : row_amc.index('assert1'),
                    'assert2' : row_amc.index('assert2'),
                    'ella' : row_amc.index('ella'),
                    'fc1' : row_amc.index('fc1'),
                    'fdp1' : row_amc.index('fdp1'),
                    'lecture11' : row_amc.index('lecture11'),
                    'lecture31' : row_amc.index('lecture31'),
                    'mathbis131' : row_amc.index('mathbis131'),
                    'maths115' : row_amc.index('maths115'),
                    'maths12' : row_amc.index('maths12'),
                    'maths123' : row_amc.index('maths123'),
                    'maths126' : row_amc.index('maths126'),
                    'maths17' : row_amc.index('maths17'),
                    'maths19' : row_amc.index('maths19'),
                    'maths224' : row_amc.index('maths224'),
                    'maths225' : row_amc.index('maths225'),
                    'mathsbis11' : row_amc.index('mathsbis11'),
                    'mathsbis111' : row_amc.index('mathsbis111'),
                    'mathsbis119' : row_amc.index('mathsbis119'),
                    'mathsbis120' : row_amc.index('mathsbis120'),
                    'mathsbis16' : row_amc.index('mathsbis16'),
                    'mathsbis215' : row_amc.index('mathsbis215'),
                    'mathsbis28b' : row_amc.index('mathsbis28b'),
                    'mp22' : row_amc.index('mp22'),
                    'mp42' : row_amc.index('mp42'),
                    'phrec21' : row_amc.index('phrec21'),
                    'phrec41' : row_amc.index('phrec41'),
                    'pytha42' : row_amc.index('pytha42'),
                    'pytha43' : row_amc.index('pytha43'),
                    'testlog1' : row_amc.index('testlog1'),
                    'thales32' : row_amc.index('thales32'),
                    'thales42' : row_amc.index('thales42'),
                    'vec82' : row_amc.index('vec82')
                }
            else:
                tutor_file = open(path_tutor_file)
                tutor_reader = csv.reader(tutor_file, delimiter=',')
                is_match = False
                for id_row_tutor, row_tutor in enumerate(tutor_reader):
                    if(id_row_tutor == 0):
                        header_tutor_csv = {
                            'tutor' : row_tutor.index('Conseiller'),
                            'tutor_mail' : row_tutor.index('Email Conseiller'),
                            'name' : row_tutor.index('Nom'),
                            'firstname' : row_tutor.index('Prénom'),
                            'student_mail' : row_tutor.index('Email')
                        }
                    else:
                        full_name = row_tutor[header_tutor_csv['firstname']].lower() + ' ' + row_tutor[header_tutor_csv['name']].lower()
                        if(full_name == row_amc[header_amc_csv['name']].lower()):
                            is_match = True 
                            break
                if(is_match):
                    row_to_write = {
                        'Nom' : row_tutor[header_tutor_csv['name']],
                        'surname' : row_tutor[header_tutor_csv['firstname']],
                        'Tuteur' : row_tutor[header_tutor_csv['tutor_mail']],
                        'Email' : row_tutor[header_tutor_csv['student_mail']],
                        'note' : row_amc[header_amc_csv['note']],
                        '206' : row_amc[header_amc_csv['206']],
                        '208' : row_amc[header_amc_csv['208']],
                        '9714' : row_amc[header_amc_csv['9714']],
                        'BA1301' : row_amc[header_amc_csv['BA1301']],
                        'BA1501' : row_amc[header_amc_csv['BA1501']],
                        'BA2101' : row_amc[header_amc_csv['BA2101']],
                        'Bary22' : row_amc[header_amc_csv['Bary22']],
                        'Bary32' : row_amc[header_amc_csv['Bary32']],
                        'FLVH1' : row_amc[header_amc_csv['FLVH1']],
                        'Geo3d21' : row_amc[header_amc_csv['Geo3d21']],
                        'Geo3d41' : row_amc[header_amc_csv['Geo3d41']],
                        'OG1' : row_amc[header_amc_csv['OG1']],
                        'OG32' : row_amc[header_amc_csv['OG32']],
                        'Phvec52' : row_amc[header_amc_csv['Phvec52']],
                        'Poutres12' : row_amc[header_amc_csv['Poutres12']],
                        'Poutres41' : row_amc[header_amc_csv['Poutres41']],
                        'VH10' : row_amc[header_amc_csv['VH10']],
                        'VH6' : row_amc[header_amc_csv['VH6']],
                        'VHb2' : row_amc[header_amc_csv['VHb2']],
                        'VHbis4' : row_amc[header_amc_csv['VHbis4']],
                        'Vitesse31' : row_amc[header_amc_csv['Vitesse31']],
                        'algo1' : row_amc[header_amc_csv['algo1']],
                        'algo2' : row_amc[header_amc_csv['algo2']],
                        'assert1' : row_amc[header_amc_csv['assert1']],
                        'assert2' : row_amc[header_amc_csv['assert2']],
                        'ella' : row_amc[header_amc_csv['ella']],
                        'fc1' : row_amc[header_amc_csv['fc1']],
                        'fdp1' : row_amc[header_amc_csv['fdp1']],
                        'lecture11' : row_amc[header_amc_csv['lecture11']],
                        'lecture31' : row_amc[header_amc_csv['lecture31']],
                        'mathbis131' : row_amc[header_amc_csv['mathbis131']],
                        'maths115' : row_amc[header_amc_csv['maths115']],
                        'maths12' : row_amc[header_amc_csv['maths12']],
                        'maths123' : row_amc[header_amc_csv['maths123']],
                        'maths126' : row_amc[header_amc_csv['maths126']],
                        'maths17' : row_amc[header_amc_csv['maths17']],
                        'maths19' : row_amc[header_amc_csv['maths19']],
                        'maths224' : row_amc[header_amc_csv['maths224']],
                        'maths225' : row_amc[header_amc_csv['maths225']],
                        'mathsbis11' : row_amc[header_amc_csv['mathsbis11']],
                        'mathsbis111' : row_amc[header_amc_csv['mathsbis111']],
                        'mathsbis119' : row_amc[header_amc_csv['mathsbis119']],
                        'mathsbis120' : row_amc[header_amc_csv['mathsbis120']],
                        'mathsbis16' : row_amc[header_amc_csv['mathsbis16']],
                        'mathsbis215' : row_amc[header_amc_csv['mathsbis215']],
                        'mathsbis28b' : row_amc[header_amc_csv['mathsbis28b']],
                        'mp22' : row_amc[header_amc_csv['mp22']],
                        'mp42' : row_amc[header_amc_csv['mp42']],
                        'phrec21' : row_amc[header_amc_csv['phrec21']],
                        'phrec41' : row_amc[header_amc_csv['phrec41']],
                        'pytha42' : row_amc[header_amc_csv['pytha42']],
                        'pytha43' : row_amc[header_amc_csv['pytha43']],
                        'testlog1' : row_amc[header_amc_csv['testlog1']],
                        'thales32' : row_amc[header_amc_csv['thales32']],
                        'thales42' : row_amc[header_amc_csv['thales42']],
                        'vec82' : row_amc[header_amc_csv['vec82']]
                    }
                    merge_writer.writerow(row_to_write)




"""
'index_email' : row.index('Email'),
                        'note' : row.index('note'),
                        '206' : row.index('206'),
                        '208' : row.index('208'),
                        '9714' : row.index('9714'),
                        'BA1301' : row.index('BA1301'),
                        'BA1501' : row.index('BA1501'),
                        'BA2101' : row.index('BA2101'),
                        'Bary22' : row.index('Bary22'),
                        'Bary32' : row.index('Bary32'),
                        'FLVH1' : row.index('FLVH1'),
                        'Geo3d21' : row.index('Geo3d21'),
                        'Geo3d41' : row.index('Geo3d41'),
                        'OG1' : row.index('OG1'),
                        'OG32' : row.index('OG32'),
                        'Phvec52' : row.index('Phvec52'),
                        'Poutres12' : row.index('Poutres12'),
                        'Poutres41' : row.index('Poutres41'),
                        'VH10' : row.index('VH10'),
                        'VH6' : row.index('VH6'),
                        'VHb2' : row.index('VHb2'),
                        'VHbis4' : row.index('VHbis4'),
                        'Vitesse31' : row.index('Vitesse31'),
                        'algo1' : row.index('algo1'),
                        'algo2' : row.index('algo2'),
                        'assert1' : row.index('assert1'),
                        'assert2' : row.index('assert2'),
                        'ella' : row.index('ella'),
                        'fc1' : row.index('fc1'),
                        'fdp1' : row.index('fdp1'),
                        'lecture11' : row.index('lecture11'),
                        'lecture31' : row.index('lecture31'),
                        'mathbis131' : row.index('mathbis131'),
                        'maths115' : row.index('maths115'),
                        'maths12' : row.index('maths12'),
                        'maths123' : row.index('maths123'),
                        'maths126' : row.index('maths126'),
                        'maths17' : row.index('maths17'),
                        'maths19' : row.index('maths19'),
                        'maths224' : row.index('maths224'),
                        'maths225' : row.index('maths225'),
                        'mathsbis11' : row.index('mathsbis11'),
                        'mathsbis111' : row.index('mathsbis111'),
                        'mathsbis119' : row.index('mathsbis119'),
                        'mathsbis120' : row.index('mathsbis120'),
                        'mathsbis16' : row.index('mathsbis16'),
                        'mathsbis215' : row.index('mathsbis215'),
                        'mathsbis28b' : row.index('mathsbis28b'),
                        'mp22' : row.index('mp22'),
                        'mp42' : row.index('mp42'),
                        'phrec21' : row.index('phrec21'),
                        'phrec41' : row.index('phrec41'),
                        'pytha42' : row.index('pytha42'),
                        'pytha43' : row.index('pytha43'),
                        'testlog1' : row.index('testlog1'),
                        'thales32' : row.index('thales32'),
                        'thales42' : row.index('thales42'),
                        'vec82' : row.index('vec82') 
                    }

"""