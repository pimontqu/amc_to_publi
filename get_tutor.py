import os
import csv
from unidecode import unidecode

path_user_bac_tutor = '/home/quentinp/Documents/projets/publipostage/bac/p2021/input/tuteur.csv'
path_user_moodle = 'assets/input/p2020/Utilisateurs_moodle.csv'
path_tutor_mail = '/home/quentinp/Documents/projets/publipostage/bac/p2021/output/tutor_mail.csv'
tutor_mail_csv = open(path_tutor_mail, 'w')
tutor_mail_writer = csv.writer(tutor_mail_csv, delimiter=",")
with open(path_user_bac_tutor) as tutor_csv:
    tutor_reader = csv.reader(tutor_csv, delimiter=",")
    for nb_row, tutor in enumerate(tutor_reader):
        if(nb_row > 0):
            if(len(tutor) == 1):
                tutor_fullname = tutor[0]
                tutor_mail = ''
                with open(path_user_moodle) as moodle_csv:
                    moodle_reader = csv.reader(moodle_csv , delimiter=",")
                    for nb_row_moodle, moodle_user in enumerate(moodle_reader):
                        if(nb_row_moodle == 0):
                            email_index = moodle_user.index('email')
                            fisrtname_index = moodle_user.index('firstname')
                            lastname_index = moodle_user.index('lastname')  
                        else:
                            current_fullname = moodle_user[lastname_index] + ' ' + moodle_user[fisrtname_index]
                            if(tutor_fullname.lower() == current_fullname.lower()):
                                tutor_mail = moodle_user[email_index]
                                break
                    if(tutor_mail == ''):
                        tutor_mail = tutor_fullname
                    moodle_csv.close()
                tutor_mail_writer.writerow([tutor_mail])
            else:
                tutor_mail_writer.writerow([''])

