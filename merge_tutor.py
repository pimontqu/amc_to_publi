import csv

header_score_file = [
    'Prénom',
    'Nom',
    'Tuteur',
    'Adresse de courriel',
    'Analyser un algorithme_info NF01',
    'Analyser le mouvement d\'un corps en chute libre_phys PS21',
    'Associer une fonction dérivée à une fonction primitive_math PS21 MT90 PS90',
    'Calculer des dérivées dont des fonctions composées_math CM11 PS21 MT90 PS90',
    'Calculer des intégrales simples_math CM11 MT90',
    'Calculer module et argument d\'une opération sur les nombres complexes_math PS94 MT90',
    'Calculer des vitesses_phys PS21',
    'Comprendre l\'appartenance à un ensemble_math PS93 MT90',
    'Connaître le vocabulaire mathématique_math MT90',
    'Connaître les éléments de laboratoire_chim CM11',
    'Connaître les fonctions chimiques de base_chim CM11',
    'Connaître les fonctions et leurs dérivées en trigonométrie_math PS21 MT90',
    'Connaître les grandeurs les plus courantes_phys CM11 PS90',
    'Connaître les propriétés du pH_chim CM11',
    'Connaître les théorèmes de Thalès et Pythagore_math PS94',
    'Définir le barycentre_phys PS21',
    'Déterminer le domaine de définition d\'une fonction_math MT90',
    'Différencier les notions de masse et de poids_phys PS21 NF01'	,
    'Effectuer des opérations en géométrie 3d_math PS21 NF01',
    'Effectuer un calcul vectoriel_phys PS93 PS21',
    'Effectuer une conversion entre formulations cartésienne et polaire_phys PS94',
    'Équilibrer une réaction chimique_chim CM11',
    'Extraire des données d\'un énoncé_math PS93 MT90',
    'Identifier la croissance et la décroissante d\'une suite_math MT90',
    'Identifier la matière_chim CM11',
    'Interpréter un test logique_math NF01',
    'Maîtriser le raisonnement par récurrence_math MT90',
    'Maîtriser les expressions algébriques_math MT90',
    'Manipuler des assertions logiques_math NF01',
    'Reconnaître la continuité d\'une fonction_math MT90',
    'Reconnaître la courbe représentative d\'une fonction_math MT90',
    'Reconnaître une suite géométrique ou arithmétique_math MT90',
    'Tester le principe de linéarité_math MT90',
    'Trouver la forme canonique d\'un polynôme_math MT90',
    'Utiliser les fonctions logarithme, exponentielle et puissance_math MT90 NF01 CM11',
    'Définir le PGCD et PPMC_math MT90 NF01',
    'Définir le centre de gravité_phys PS21',
    'Total du cours'
]
with open('assets/output/correct/notefull.csv', 'w') as merge_tutor_csv:
    merge_tutor_writer = csv.DictWriter(merge_tutor_csv, fieldnames=header_score_file, quoting = csv.QUOTE_NONNUMERIC)
    merge_tutor_writer.writeheader()
    with open('assets/output/correct/note.csv') as score_csv:
        score_reader = csv.reader(score_csv, delimiter=',')
        for index_row_score, row_score in enumerate(score_reader):
            if(index_row_score == 0):
                index_score_csv = {
                    'firstname' : row_score.index('Prénom'),
                    'lastname' : row_score.index('Nom'),
                    'tutor' : row_score.index('Tuteur'),
                    'email' : row_score.index('Adresse de courriel'),
                    'test1' : row_score.index('Analyser un algorithme_info NF01'),
                    'test2' : row_score.index('Analyser le mouvement d\'un corps en chute libre_phys PS21'),
                    'test3' : row_score.index('Associer une fonction dérivée à une fonction primitive_math PS21 MT90 PS90'),
                    'test4' : row_score.index('Calculer des dérivées dont des fonctions composées_math CM11 PS21 MT90 PS90'),
                    'test5' : row_score.index('Calculer des intégrales simples_math CM11 MT90'),
                    'test6' : row_score.index('Calculer module et argument d\'une opération sur les nombres complexes_math PS94 MT90'),
                    'test7' : row_score.index('Calculer des vitesses_phys PS21'),
                    'test8' : row_score.index('Comprendre l\'appartenance à un ensemble_math PS93 MT90'),
                    'test9' : row_score.index('Connaître le vocabulaire mathématique_math MT90'),
                    'test10' : row_score.index('Connaître les éléments de laboratoire_chim CM11'),
                    'test11' : row_score.index('Connaître les fonctions chimiques de base_chim CM11'),
                    'test12' : row_score.index('Connaître les fonctions et leurs dérivées en trigonométrie_math PS21 MT90'),
                    'test13' : row_score.index('Connaître les grandeurs les plus courantes_phys CM11 PS90'),
                    'test14' : row_score.index('Connaître les propriétés du pH_chim CM11'),
                    'test15' : row_score.index('Connaître les théorèmes de Thalès et Pythagore_math PS94'),
                    'test16' : row_score.index('Définir le barycentre_phys PS21'),
                    'test17' : row_score.index('Déterminer le domaine de définition d\'une fonction_math MT90'),
                    'test18' : row_score.index('Différencier les notions de masse et de poids_phys PS21 NF01'	),
                    'test19' : row_score.index('Effectuer des opérations en géométrie 3d_math PS21 NF01'),
                    'test20' : row_score.index('Effectuer un calcul vectoriel_phys PS93 PS21'),
                    'test21' : row_score.index('Effectuer une conversion entre formulations cartésienne et polaire_phys PS94'),
                    'test22' : row_score.index('Équilibrer une réaction chimique_chim CM11'),
                    'test23' : row_score.index('Extraire des données d\'un énoncé_math PS93 MT90'),
                    'test24' : row_score.index('Identifier la croissance et la décroissante d\'une suite_math MT90'),
                    'test25' : row_score.index('Identifier la matière_chim CM11'),
                    'test26' : row_score.index('Interpréter un test logique_math NF01'),
                    'test27' : row_score.index('Maîtriser le raisonnement par récurrence_math MT90'),
                    'test28' : row_score.index('Maîtriser les expressions algébriques_math MT90'),
                    'test29' : row_score.index('Manipuler des assertions logiques_math NF01'),
                    'test30' : row_score.index('Reconnaître la continuité d\'une fonction_math MT90'),
                    'test31' : row_score.index('Reconnaître la courbe représentative d\'une fonction_math MT90'),
                    'test32' : row_score.index('Reconnaître une suite géométrique ou arithmétique_math MT90'),
                    'test33' : row_score.index('Tester le principe de linéarité_math MT90'),
                    'test34' : row_score.index('Trouver la forme canonique d\'un polynôme_math MT90'),
                    'test35' : row_score.index('Utiliser les fonctions logarithme, exponentielle et puissance_math MT90 NF01 CM11'),
                    'test36' : row_score.index('Définir le PGCD et PPMC_math MT90 NF01'),
                    'test37' : row_score.index('Définir le centre de gravité_phys PS21'),
                    'total' : row_score.index('Total du cours')
                }
            else:
                email_student_score_csv = row_score[index_score_csv['email']]
                with open('assets/input/a2019/input_for_amc.csv') as tutor_csv:
                    tutor_reader =csv.reader(tutor_csv, delimiter=',')
                    for index_row_tutor, row_tutor in enumerate(tutor_reader):
                        if(index_row_tutor == 0):
                            index_tutor_csv = {
                                'tutor_mail' : row_tutor.index('Email Conseiller'),
                                'student_mail' : row_tutor.index('Email')
                            }
                        else:
                            if(email_student_score_csv == row_tutor[index_tutor_csv['student_mail']]):
                                tutor_mail = row_tutor[index_tutor_csv['tutor_mail']]
                                break
                row_to_write = {
                    'Prénom' : row_score[index_score_csv['firstname']],
                    'Nom' : row_score[index_score_csv['lastname']],
                    'Tuteur' : tutor_mail,
                    'Adresse de courriel' : row_score[index_score_csv['email']],
                    'Analyser un algorithme_info NF01' : row_score[index_score_csv['test1']],
                    'Analyser le mouvement d\'un corps en chute libre_phys PS21' : row_score[index_score_csv['test2']],
                    'Associer une fonction dérivée à une fonction primitive_math PS21 MT90 PS90' : row_score[index_score_csv['test3']],
                    'Calculer des dérivées dont des fonctions composées_math CM11 PS21 MT90 PS90' : row_score[index_score_csv['test4']],
                    'Calculer des intégrales simples_math CM11 MT90' : row_score[index_score_csv['test5']],
                    'Calculer module et argument d\'une opération sur les nombres complexes_math PS94 MT90' : row_score[index_score_csv['test6']],
                    'Calculer des vitesses_phys PS21' : row_score[index_score_csv['test7']],
                    'Comprendre l\'appartenance à un ensemble_math PS93 MT90' : row_score[index_score_csv['test8']],
                    'Connaître le vocabulaire mathématique_math MT90' : row_score[index_score_csv['test9']],
                    'Connaître les éléments de laboratoire_chim CM11' : row_score[index_score_csv['test10']],
                    'Connaître les fonctions chimiques de base_chim CM11' : row_score[index_score_csv['test11']],
                    'Connaître les fonctions et leurs dérivées en trigonométrie_math PS21 MT90' : row_score[index_score_csv['test12']],
                    'Connaître les grandeurs les plus courantes_phys CM11 PS90' : row_score[index_score_csv['test13']],
                    'Connaître les propriétés du pH_chim CM11' : row_score[index_score_csv['test14']],
                    'Connaître les théorèmes de Thalès et Pythagore_math PS94' : row_score[index_score_csv['test15']],
                    'Définir le barycentre_phys PS21' : row_score[index_score_csv['test16']],
                    'Déterminer le domaine de définition d\'une fonction_math MT90' : row_score[index_score_csv['test17']],
                    'Différencier les notions de masse et de poids_phys PS21 NF01'	 : row_score[index_score_csv['test18']],
                    'Effectuer des opérations en géométrie 3d_math PS21 NF01' : row_score[index_score_csv['test19']],
                    'Effectuer un calcul vectoriel_phys PS93 PS21' : row_score[index_score_csv['test20']],
                    'Effectuer une conversion entre formulations cartésienne et polaire_phys PS94' : row_score[index_score_csv['test21']],
                    'Équilibrer une réaction chimique_chim CM11' : row_score[index_score_csv['test22']],
                    'Extraire des données d\'un énoncé_math PS93 MT90' : row_score[index_score_csv['test23']],
                    'Identifier la croissance et la décroissante d\'une suite_math MT90' : row_score[index_score_csv['test24']],
                    'Identifier la matière_chim CM11' : row_score[index_score_csv['test25']],
                    'Interpréter un test logique_math NF01' : row_score[index_score_csv['test26']],
                    'Maîtriser le raisonnement par récurrence_math MT90' : row_score[index_score_csv['test27']],
                    'Maîtriser les expressions algébriques_math MT90' : row_score[index_score_csv['test28']],
                    'Manipuler des assertions logiques_math NF01' : row_score[index_score_csv['test29']],
                    'Reconnaître la continuité d\'une fonction_math MT90' : row_score[index_score_csv['test30']],
                    'Reconnaître la courbe représentative d\'une fonction_math MT90' : row_score[index_score_csv['test31']],
                    'Reconnaître une suite géométrique ou arithmétique_math MT90' : row_score[index_score_csv['test32']],
                    'Tester le principe de linéarité_math MT90' : row_score[index_score_csv['test33']],
                    'Trouver la forme canonique d\'un polynôme_math MT90' : row_score[index_score_csv['test34']],
                    'Utiliser les fonctions logarithme, exponentielle et puissance_math MT90 NF01 CM11' : row_score[index_score_csv['test35']],
                    'Définir le PGCD et PPMC_math MT90 NF01' : row_score[index_score_csv['test36']],
                    'Définir le centre de gravité_phys PS21' : row_score[index_score_csv['test37']],
                    'Total du cours' : row_score[index_score_csv['total']]
                }
                merge_tutor_writer.writerow(row_to_write)