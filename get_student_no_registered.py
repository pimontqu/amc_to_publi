import csv
import sys
from lxml import etree

xml_path = ''
csv_path = ''
student_not_register = []
xml_student = etree.parse('assets/input/a2019/inscrits_UV_A2019(4).xml')
list_uv = xml_student.xpath('/ExtractionInscriptions/ListeEnseignements/Enseignement')
with open('assets/input/a2019/bac_a19.csv') as amc_csv :
    amc_reader = csv.reader(amc_csv, delimiter=',')
    for index_row, row in enumerate(amc_reader):
        if(index_row == 0):
            index_mail = row.index('Email')
        else:
            student_finded = False
            current_mail_student = row[index_mail]
            if(current_mail_student != ''):
                for uv in list_uv:
                    list_student = uv.xpath('ListeInscrits/Inscrit')
                    for student in list_student:
                        if(current_mail_student == student.xpath('Email')[0].text):
                            student_finded = True
                            break
                    if(student_finded):
                        break
                if(not student_finded):
                    student_not_register.append(current_mail_student)
print(student_not_register)
print(len(student_not_register))