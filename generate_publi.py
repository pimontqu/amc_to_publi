# coding=utf-8
from lxml import etree
from tkinter import *
from tkinter import ttk
from tkinter import filedialog
import csv
import os


class GuiPubli():
    student_tested = []
    path_output_dir = ''

    def __init__(self):
        # header du fichernote.csv
        self.header_score_file = ['Prénom', 'Nom', 'Tuteur', 'Adresse de courriel', 'Analyser un algorithme_info NF01', 'Analyser le mouvement d\'un corps en chute libre_phys PS21', 
        'Associer une fonction dérivée à une fonction primitive_math PS21 MT90 PS90', 'Calculer des dérivées dont des fonctions composées_math CM11 PS21 MT90 PS90', 'Calculer des intégrales simples_math CM11 MT90',
        'Calculer module et argument d\'une opération sur les nombres complexes_math PS94 MT90', 'Calculer des vitesses_phys PS21', 'Comprendre l\'appartenance à un ensemble_math PS93 MT90',
        'Connaître le vocabulaire mathématique_math MT90', 'Connaître les éléments de laboratoire_chim CM11', 'Connaître les fonctions chimiques de base_chim CM11', 
        'Connaître les fonctions et leurs dérivées en trigonométrie_math PS21 MT90', 'Connaître les grandeurs les plus courantes_phys CM11 PS90', 'Connaître les propriétés du pH_chim CM11', 
        'Connaître les théorèmes de Thalès et Pythagore_math PS94', 'Définir le barycentre_phys PS21', 'Déterminer le domaine de définition d\'une fonction_math MT90',	
        'Différencier les notions de masse et de poids_phys PS21 NF01',	'Effectuer des opérations en géométrie 3d_math PS21 NF01', 'Effectuer un calcul vectoriel_phys PS93 PS21', 
        'Effectuer une conversion entre formulations cartésienne et polaire_phys PS94', 'Équilibrer une réaction chimique_chim CM11', 'Extraire des données d\'un énoncé_math PS93 MT90',
        'Identifier la croissance et la décroissante d\'une suite_math MT90', 'Identifier la matière_chim CM11', 'Interpréter un test logique_math NF01', 'Maîtriser le raisonnement par récurrence_math MT90', 
        'Maîtriser les expressions algébriques_math MT90', 'Manipuler des assertions logiques_math NF01', 'Reconnaître la continuité d\'une fonction_math MT90', 
        'Reconnaître la courbe représentative d\'une fonction_math MT90', 'Reconnaître une suite géométrique ou arithmétique_math MT90', 'Tester le principe de linéarité_math MT90', 
        'Trouver la forme canonique d\'un polynôme_math MT90', 'Utiliser les fonctions logarithme, exponentielle et puissance_math MT90 NF01 CM11',	
        'Définir le PGCD et PPMC_math MT90 NF01', 'Définir le centre de gravité_phys PS21',	'Total du cours']
        # liste des uv testé
        self.uvs_tested = ['CM11', 'MT90', 'NF01', 'PS21', 'PS90', 'PS93', 'PS94']
        # label pour les input file et dir de l'interface
        self.path_xml_student = 'Renseigner le fichier des étudiants'
        self.path_amc = 'Renseigner le fichier amc'
        self.path_output_dir = 'Renseigner le dossier ou seront\nenregistré les fichiers généré'
        # texte qui signifira la fin de la génération
        self.progress_text = ''
    # récupère le dossier où les fichiers générés seront enregistré
    def set_output_dir(self):
        self.path_output_dir = filedialog.askdirectory() + '/'
        self.label_output_dir.config(text = self.path_output_dir)
    # récupère le fichier de amc
    def set_amc_file(self):
        self.path_amc = filedialog.askopenfile().name
        self.label_amc_file.config(text = self.path_amc)
    # recupère le chemin du fichier xml
    def set_xml_etu(self):
        self.path_xml_student = filedialog.askopenfile().name
        self.label_xml_etudiant.config(text = self.path_xml_student)
    # parse le xml
    def get_trees(self):
        print(self.path_xml_student)
        self.tree = etree.parse(self.path_xml_student)
        self.list_uvs = self.tree.xpath('/ExtractionInscriptions/ListeEnseignements/Enseignement')
        self.semester = self.tree.xpath('/ExtractionInscriptions/Semestre')[0].text
    # crée le fichier note
    def create_score_file(self):
        # créer le g=ficher note.csv dans le dossier renseigné
        csv_score = open(self.path_output_dir + 'note.csv', 'w')
        # crée l'objet qui ecrira dans note.csv
        writer_csv_score = csv.DictWriter(csv_score, fieldnames = self.header_score_file, quoting = csv.QUOTE_NONNUMERIC)
        # ecrit le header du fichier note.csv
        writer_csv_score.writeheader()
        # ouvre le fichier générez par amc
        with open(self.path_amc) as amc_csv :
            # convertit le fichier en liste 2d
            read_csv = csv.reader(amc_csv, delimiter=',')
            # parcour toutes les lignes du csv
            for index_row, row in enumerate(read_csv):
                # si c'est le header (première ligne)
                if(index_row == 0):
                    if ('Nom' in row):
                        name_merged = True
                    else:
                        name_merged = False
                    # recupère les index de toutes les questions,le nom, le mail et la note final
                    all_index = {
                        'index_name' : row.index('Nom') if name_merged else row.index('name'),
                        'index_surname' : '' if name_merged else row.index('surname'),
                        'index_email' : row.index('Email'),
                        'note' : row.index('note'),
                        'tutor' : row.index('tutor'),
                        '206' : row.index('206'),
                        '208' : row.index('208'),
                        '9714' : row.index('9714'),
                        'BA1301' : row.index('BA1301'),
                        'BA1501' : row.index('BA1501'),
                        'BA2101' : row.index('BA2101'),
                        'Bary22' : row.index('Bary22'),
                        'Bary32' : row.index('Bary32'),
                        'FLVH1' : row.index('FLVH1'),
                        'Geo3d21' : row.index('Geo3d21'),
                        'Geo3d41' : row.index('Geo3d41'),
                        'OG1' : row.index('OG1'),
                        'OG32' : row.index('OG32'),
                        'Phvec52' : row.index('Phvec52'),
                        'Poutres12' : row.index('Poutres12'),
                        'Poutres41' : row.index('Poutres41'),
                        'VH10' : row.index('VH10'),
                        'VH6' : row.index('VH6'),
                        'VHb2' : row.index('VHb2'),
                        'VHbis4' : row.index('VHbis4'),
                        'Vitesse31' : row.index('Vitesse31'),
                        'algo1' : row.index('algo1'),
                        'algo2' : row.index('algo2'),
                        'assert1' : row.index('assert1'),
                        'assert2' : row.index('assert2'),
                        'ella' : row.index('ella'),
                        'fc1' : row.index('fc1'),
                        'fdp1' : row.index('fdp1'),
                        'lecture11' : row.index('lecture11'),
                        'lecture31' : row.index('lecture31'),
                        'mathbis131' : row.index('mathbis131'),
                        'maths115' : row.index('maths115'),
                        'maths12' : row.index('maths12'),
                        'maths123' : row.index('maths123'),
                        'maths126' : row.index('maths126'),
                        'maths17' : row.index('maths17'),
                        'maths19' : row.index('maths19'),
                        'maths224' : row.index('maths224'),
                        'maths225' : row.index('maths225'),
                        'mathsbis11' : row.index('mathsbis11'),
                        'mathsbis111' : row.index('mathsbis111'),
                        'mathsbis119' : row.index('mathsbis119'),
                        'mathsbis120' : row.index('mathsbis120'),
                        'mathsbis16' : row.index('mathsbis16'),
                        'mathsbis215' : row.index('mathsbis215'),
                        'mathsbis28b' : row.index('mathsbis28b'),
                        'mp22' : row.index('mp22'),
                        'mp42' : row.index('mp42'),
                        'phrec21' : row.index('phrec21'),
                        'phrec41' : row.index('phrec41'),
                        'pytha42' : row.index('pytha42'),
                        'pytha43' : row.index('pytha43'),
                        'testlog1' : row.index('testlog1'),
                        'thales32' : row.index('thales32'),
                        'thales42' : row.index('thales42'),
                        'vec82' : row.index('vec82') 
                    }
                # si c'est pas le header
                else:
                    # ci la ligne est un etudiant
                    if(row[all_index['index_name']]!= ''):
                        if (row[all_index['note']] != 'ABS'):
                            if (name_merged):
                                # indique si l'étudiant à été trouvé
                                student_finded = False
                                # récupération du nom complet de l'étudiant
                                full_name_current_student = row[all_index['index_name']]
                                # préparation des variable pour le nom et le prenom de l'etudiant séparé
                                first_name_current_stud = ''
                                last_name_current_stud = ''
                                # email du tuteur
                                current_tutor_stud = row[all_index['tutor']]
                                #current_tutor_stud = ''
                                # email de l'etudiant
                                email_current_stud = row[all_index['index_email']]
                                # pour chaque uv
                                for ev in self.list_uvs :
                                    # on récupère les étudiant de cet uv puis on les parcours
                                    for student_ev in ev.xpath('ListeInscrits/Inscrit') :
                                        # si le nom ou l'email correspond à ceux de l'etudiant courant
                                        if(full_name_current_student == student_ev.xpath('Prenom')[0].text + ' ' + student_ev.xpath('Nom')[0].text or email_current_stud == student_ev.xpath('Email')[0].text):
                                            # on recupère son nom et prénom
                                            first_name_current_stud = student_ev.xpath('Prenom')[0].text
                                            last_name_current_stud = student_ev.xpath('Nom')[0].text
                                            # on ajoute le mail dans une liste qui servira pour le fichier resp.csv
                                            self.student_tested.append(email_current_stud)
                                            # on indique que l'etudiant à été trouvé
                                            student_finded = True
                                            # on arrête le parcours des étudiants de cet uv
                                            break
                                    # si l'etudiant à été trouvé
                                    if(student_finded):
                                        # on arrète le parcours des uvs
                                        break
                            else :
                                first_name_current_stud = row[all_index['index_surname']]
                                last_name_current_stud = row[all_index['index_name']]
                                email_current_stud = row[all_index['index_email']]
                                current_tutor_stud = row[all_index['tutor']]
                                #current_tutor_stud = ''
                                self.student_tested.append(email_current_stud)
                            # on prépare la ligne à ecrire pour l'étudiants avec son nom, prénoms, mails, et le calcul des notes pour chaque catégories.
                            current_row = {
                                'Prénom' : first_name_current_stud,
                                'Nom' : last_name_current_stud,
                                'Tuteur' : current_tutor_stud,
                                'Adresse de courriel' : email_current_stud,
                                'Analyser un algorithme_info NF01' : (self.convert_int(row[all_index['algo1']]) + self.convert_int(row[all_index['algo2']])) * 100 / 2,
                                'Analyser le mouvement d\'un corps en chute libre_phys PS21' :  self.convert_int(row[all_index['9714']]) * 100 / 1,
                                'Associer une fonction dérivée à une fonction primitive_math PS21 MT90 PS90' : self.convert_int(row[all_index['fdp1']]) * 100 / 1,
                                'Calculer des dérivées dont des fonctions composées_math CM11 PS21 MT90 PS90' : (self.convert_int(row[all_index['mathsbis119']]) + self.convert_int(row[all_index['mathsbis120']])) * 100 / 2,
                                'Calculer des intégrales simples_math CM11 MT90' : self.convert_int(row[all_index['mathsbis11']]) * 100 / 1,
                                'Calculer module et argument d\'une opération sur les nombres complexes_math PS94 MT90' : self.convert_int(row[all_index['mathsbis28b']]) * 100 / 1,
                                'Calculer des vitesses_phys PS21' : self.convert_int(row[all_index['Vitesse31']]) * 100 / 1,
                                'Comprendre l\'appartenance à un ensemble_math PS93 MT90' : self.convert_int(row[all_index['VH6']]) * 100 / 1,
                                'Connaître le vocabulaire mathématique_math MT90' : (self.convert_int(row[all_index['VH10']]) + self.convert_int(row[all_index['VHb2']])) * 100 / 2,
                                'Connaître les éléments de laboratoire_chim CM11' : self.convert_int(row[all_index['ella']]) * 100 / 1,
                                'Connaître les fonctions chimiques de base_chim CM11' : self.convert_int(row[all_index['fc1']]) * 100 / 1,
                                'Connaître les fonctions et leurs dérivées en trigonométrie_math PS21 MT90' : (self.convert_int(row[all_index['maths115']]) + self.convert_int(row[all_index['maths19']])) * 100 / 2,
                                'Connaître les grandeurs les plus courantes_phys CM11 PS90' : (self.convert_int(row[all_index['OG1']]) + self.convert_int(row[all_index['OG32']])) * 100 / 2,
                                'Connaître les propriétés du pH_chim CM11' : self.convert_int(row[all_index['BA1501']]) * 100 / 1,
                                'Connaître les théorèmes de Thalès et Pythagore_math PS94' : (self.convert_int(row[all_index['pytha42']]) + self.convert_int(row[all_index['pytha43']]) + self.convert_int(row[all_index['thales32']]) + self.convert_int(row[all_index['thales42']])) * 100 / 4,
                                'Définir le barycentre_phys PS21' : (self.convert_int(row[all_index['Bary22']]) + self.convert_int(row[all_index['Bary32']])) * 100 / 2,
                                'Déterminer le domaine de définition d\'une fonction_math MT90' : self.convert_int(row[all_index['VHbis4']]) * 100 / 1,
                                'Différencier les notions de masse et de poids_phys PS21 NF01' : (self.convert_int(row[all_index['mp22']]) + self.convert_int(row[all_index['mp42']])) * 100 / 2,
                                'Effectuer des opérations en géométrie 3d_math PS21 NF01' : (self.convert_int(row[all_index['Geo3d21']]) + self.convert_int(row[all_index['Geo3d41']])) * 100 / 2,
                                'Effectuer un calcul vectoriel_phys PS93 PS21' : (self.convert_int(row[all_index['vec82']]) + self.convert_int(row[all_index['Phvec52']])) * 100 / 2,
                                'Effectuer une conversion entre formulations cartésienne et polaire_phys PS94' : self.convert_int(row[all_index['mathsbis111']]) * 100 / 1,
                                'Équilibrer une réaction chimique_chim CM11' : self.convert_int(row[all_index['BA1301']]) * 100 / 1,
                                'Extraire des données d\'un énoncé_math PS93 MT90' : (self.convert_int(row[all_index['lecture11']]) + self.convert_int(row[all_index['lecture31']])) * 100 / 2,
                                'Identifier la croissance et la décroissante d\'une suite_math MT90' : (self.convert_int(row[all_index['maths12']]) + self.convert_int(row[all_index['maths126']])) * 100 / 2,
                                'Identifier la matière_chim CM11' : self.convert_int(row[all_index['BA2101']]) * 100 / 1,
                                'Interpréter un test logique_math NF01' : self.convert_int(row[all_index['testlog1']]) * 100 / 1,
                                'Maîtriser le raisonnement par récurrence_math MT90' : (self.convert_int(row[all_index['phrec21']]) +  self.convert_int(row[all_index['phrec41']])) * 100 / 2,
                                'Maîtriser les expressions algébriques_math MT90' : self.convert_int(row[all_index['mathsbis16']]) * 100 / 1,
                                'Manipuler des assertions logiques_math NF01' : (self.convert_int(row[all_index['assert1']]) + self.convert_int(row[all_index['assert2']])) * 100 / 2,
                                'Reconnaître la continuité d\'une fonction_math MT90' : self.convert_int(row[all_index['mathsbis215']]) * 100 / 1,
                                'Reconnaître la courbe représentative d\'une fonction_math MT90' : (self.convert_int(row[all_index['206']]) + self.convert_int(row[all_index['208']])) * 100 / 2,
                                'Reconnaître une suite géométrique ou arithmétique_math MT90' : self.convert_int(row[all_index['maths123']]) * 100 / 1,
                                'Tester le principe de linéarité_math MT90' : self.convert_int(row[all_index['mathbis131']]) * 100 / 1,
                                'Trouver la forme canonique d\'un polynôme_math MT90' : self.convert_int(row[all_index['FLVH1']]) * 100 / 1,
                                'Utiliser les fonctions logarithme, exponentielle et puissance_math MT90 NF01 CM11' : self.convert_int(row[all_index['maths17']]) * 100 / 1,
                                'Définir le PGCD et PPMC_math MT90 NF01' : (self.convert_int(row[all_index['maths224']]) + self.convert_int(row[all_index['maths225']])) * 100 / 2,
                                'Définir le centre de gravité_phys PS21' : (self.convert_int(row[all_index['Poutres12']]) + self.convert_int(row[all_index['Poutres41']])) * 100 / 2,
                                'Total du cours' : self.convert_float(row[all_index['note']]) * 5
                            }
                            # puis on ecrire la ligne
                            writer_csv_score.writerow(current_row)
    # crée le fichier responsable
    def create_resp_file(self):
        # liste qui sera utiliser pour chaque row
        all_code = ['id']
        all_up_group = ['groupe supérieur']
        all_title = ['nom']
        all_resp = ['responsable']
        all_etu = []
        # créer le nouveaux fichier resp.csv
        with open(self.path_output_dir + 'resp.csv', 'w') as resp_csv:
            # objet qui permet d'ecrire les csv
            resp_csv_writer = csv.writer(resp_csv, quoting = csv.QUOTE_NONNUMERIC)
            # pour chaque uv
            for uv in self.list_uvs:
                # code/id de l'uv
                code_uv = uv.xpath('Code')[0].text
                # si l'uv est dans les uv testé
                if(code_uv in self.uvs_tested):
                    # contiendras tous les mails des étudiants.
                    mails_student_uv = []
                    # ajout du code dans la liste des score
                    all_code.append(code_uv)
                    # ajout d'un up_group vide pour l'uv courante
                    all_up_group.append('')
                    # titre de l'uv
                    title_uv = uv.xpath('Titre')[0].text.lower()
                    # mail du responsable
                    mail_resp = uv.xpath('Responsable/Email')[0].text
                    # ajout du titre et du mail du resp dans leur liste respective
                    all_title.append(title_uv)
                    all_resp.append(mail_resp)
                    # étudiant dans l'uv
                    student_uv = uv.xpath('ListeInscrits/Inscrit')
                    # pour chaque etudiant
                    for student in student_uv:
                        # recupère l'email de l'étudiant courant
                        mail_current_stud = student.xpath('Email')[0].text
                        # si l'etudiant a passé le test
                        if(mail_current_stud in self.student_tested):
                            # on ajoutes son mail dans une liste
                            mails_student_uv.append(mail_current_stud)
                    # ajout de la liste dans une autre liste qui contient toutes les listes des mails de chaque uv
                    all_etu.append(mails_student_uv)
            # écrit les 3ère lignes du resp.csv(id/code, titre et responsable)
            resp_csv_writer.writerow(all_code)
            resp_csv_writer.writerow(all_up_group)
            resp_csv_writer.writerow(all_title)
            resp_csv_writer.writerow(all_resp)
            # indique s'il y à encore des étudiant à écrire dans le fichier csv
            remaining_student = True
            # le nombre de ligne d'etudiant ecrite
            row_index = 0
            # le nombre maximum d'etudiant dans une uv
            max_len = 0
            # tant qu'il reste des etudiants à ecrire
            while remaining_student:
                # ligne à ecrire dans les fichier
                current_row = ['étudiant']
                # pour chaque liste d'etudiant dans
                for list_student in all_etu:
                    # si c'est la premiere ligne
                    if(row_index == 0):
                        # longueur de la liste courante
                        current_len = len(list_student)
                        # si current_len est supèrieure à max_len alors max_len egale current_len
                        max_len = current_len if current_len > max_len else max_len
                    # on test si on a pas dépassé le nombre d'etudiant de cette liste
                    try:
                        col_value = list_student[row_index]
                    except IndexError:
                        col_value = ''
                    # ajout de la valeur a la ligne a ecrire 
                    current_row.append(col_value)
                # ecriture de la ligne
                resp_csv_writer.writerow(current_row)
                # incremente le nombre de ligne écrite
                row_index += 1
                # si on dépassé le nombre d'étudiant
                if(row_index >= max_len):
                    # on dit qu'il ne reste plus d'étudiant
                    remaining_student = False
    # créer l'interface graphique
    def make_gui(self):
        # initiation de l'interface graphique
        self.window = Tk()
        # création de tous les boutons et labels de l'interface
        self.label_xml_etudiant = Label(self.window, text=self.path_xml_student)
        button_xml_etudiant = Button(self.window, text='sélectionner un fichier', command=self.set_xml_etu)
        self.label_amc_file = Label(self.window, text=self.path_amc)
        button_amc_file = Button(self.window, text='sélectionner un fichier', command=self.set_amc_file)
        self.label_output_dir = Label(self.window, text=self.path_output_dir)
        button_output_dir = Button(self.window, text='sélectionner un dossier', command=self.set_output_dir)
        self.buton_generate = Button(self.window, text='générer les fichiers', command=self.generate)
        self.progress = Label(self.window, text=self.progress_text)
        # positionne tous les éléments de l'interface 
        self.label_xml_etudiant.grid(row = 1, column = 1)
        button_xml_etudiant.grid(row = 1, column = 3)
        self.label_amc_file.grid(row = 3, column = 1)
        button_amc_file.grid(row = 3, column = 3)
        self.label_output_dir.grid(row = 5, column = 1)
        button_output_dir.grid(row = 5, column = 3)
        self.buton_generate.grid(row = 7, column = 2)
        self.progress.grid(row = 9, column = 2)
        self.window.mainloop()
    # lance toute les function pour génerer les fichier
    def generate(self):
        self.progress_text = 'Géneration du fichier de note'
        self.progress.config(text = self.progress_text)
        self.get_trees()
        self.create_score_file()
        self.progress_text = 'Géneration du fichier du répartition'
        self.progress.config(text = self.progress_text)
        self.create_resp_file()
        self.progress_text = 'La génération a été effectué avec succées'
        self.progress.config(text = self.progress_text)
    # convert en int le contenue du cellule d'un csv
    def convert_int(slef, csv_cell):
        if(csv_cell == ''):
            value = 0
        else:
            value = int(csv_cell)
        return value
    # convert en float le contenue cellule d'un csv
    def convert_float(slef, csv_cell):
        return float(csv_cell.replace(',', '.'))